variable "type" {
  type        = string
  description = "(Optional) The type of load balancer to create. Possible values are `application`, `gateway`, or `network`. The default value is `application`."
  #default     = "application"
  default     = "network"

  validation {
    condition     = contains(["application", "gateway", "network"], var.type)
    error_message = "Type of load balancer must be   `application`, `gateway`, or `network`."
  }
}