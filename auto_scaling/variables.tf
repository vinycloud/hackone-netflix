variable "region" {
  description = "Região da AWS"
  type        = string
  default     = "us-east-1"
}

variable "volume_size" {
  description = "Definições de ebs"
  default     = 30
}

variable "inbound_roles" {
  description = "security group tcp and/or udp"
  type        = map(object({ to_port = number, description = string, protocol = string, cidr_blocks = list(string) }))
  default = {
    3389 = { to_port = 3389, description = "inbound RDP", protocol = "tcp", cidr_blocks = ["172.16.0.0/12"] }
    80   = { to_port = 80, description = "inbound http", protocol = "tcp", cidr_blocks = ["172.16.0.0/12"] }
    443  = { to_port = 443, description = "inbound https", protocol = "tcp", cidr_blocks = ["172.16.0.0/12"] }
  }
}

