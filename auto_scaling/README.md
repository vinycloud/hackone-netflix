## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.68 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 2.68 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_asg"></a> [asg](#module\_asg) | ./module/asg | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_lb_target_group.http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_target_group) | data source |
| [aws_subnets.all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_inbound_roles"></a> [inbound\_roles](#input\_inbound\_roles) | security group tcp and/or udp | `map(object({ to_port = number, description = string, protocol = string, cidr_blocks = list(string) }))` | <pre>{<br>  "3389": {<br>    "cidr_blocks": [<br>      "172.16.0.0/12"<br>    ],<br>    "description": "inbound RDP",<br>    "protocol": "tcp",<br>    "to_port": 3389<br>  },<br>  "443": {<br>    "cidr_blocks": [<br>      "172.16.0.0/12"<br>    ],<br>    "description": "inbound https",<br>    "protocol": "tcp",<br>    "to_port": 443<br>  },<br>  "80": {<br>    "cidr_blocks": [<br>      "172.16.0.0/12"<br>    ],<br>    "description": "inbound http",<br>    "protocol": "tcp",<br>    "to_port": 80<br>  }<br>}</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | Região da AWS | `string` | `"us-east-1"` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | Definições de ebs | `number` | `30` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | Security group id |
