resource "aws_security_group" "this" {
  name        = var.name_security_group
  description = "Allow inbound traffic"
  vpc_id      = var.vpc_id


  dynamic "ingress" {
    for_each = var.inbound_roles
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.value["to_port"]
      protocol    = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }

  egress {
    from_port   = 1025
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["172.16.0.0/12"]
    description = "Outbound all "

  }

  lifecycle {
    create_before_destroy = true
  }
}