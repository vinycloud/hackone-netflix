resource "aws_lb" "default" {
  name               = local.lb_name
  internal           = var.is_internal
  load_balancer_type = var.type
  security_groups    = local.security_group
  subnets            = var.subnet_ids

  enable_deletion_protection = false

  dynamic "access_logs" {
    for_each = var.access_logs == null ? [] : [1]
    content {
      bucket  = local.aws_s3_bucket
      prefix  = local.use_existing_s3 ? local.lb_name : null
      enabled = true
    }
  }

  enable_http2 = var.enable_http2

  tags = merge(var.tags, local.tag_default, local.tag_access_logs)

  depends_on = [
    aws_s3_bucket.access_log,
    data.aws_s3_bucket.access_log
  ]
}


resource "aws_lb_listener" "default" {
  for_each = {for cp in var.listener_ports : cp.port => cp}

  load_balancer_arn = aws_lb.default.arn
  port              = each.key
  protocol          = each.value.protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.default[each.key].arn
  }
}

resource "aws_lb_target_group" "default" {
  for_each = {for cp in var.listener_ports : cp.port => cp}

  name                 = "tg-${var.environment_name}-${var.cluster_name}-default-${each.value.port}"
  port                 = each.key
  protocol             = each.value.protocol
  vpc_id               = var.vpc_id

  tags = var.tags
}
