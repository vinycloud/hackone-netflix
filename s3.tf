## Read if exists
data "aws_s3_bucket" "access_log" {
  count = local.use_access_log && !local.create_new_s3_bucket ? 1 : 0

  bucket = var.access_logs.bucket
}

## Create
resource "aws_s3_bucket" "access_log" {
  count = local.create_new_s3_bucket ? 1 : 0

  bucket = "${var.environment_name}-${var.cluster_name}-lb-access-log-${random_id.server.hex}"

  tags = merge(var.tags, local.tag_default, {
    Name = "LB ${var.environment_name} ${var.cluster_name} Access Log"
  })
}

resource "aws_s3_bucket_public_access_block" "access_log" {
  count = local.create_new_s3_bucket ? 1 : 0

  bucket = aws_s3_bucket.access_log[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "allow_access" {
  count = local.create_new_s3_bucket ? 1 : 0

  bucket = aws_s3_bucket.access_log[0].id
  policy = data.aws_iam_policy_document.default[0].json
}


## Add Lifecycle Rule
resource "aws_s3_bucket_lifecycle_configuration" "bucket_config" {
  count = local.create_new_s3_bucket && local.create_s3_lifecycle ? 1 : 0


  bucket = local.aws_s3_bucket

  rule {
    id = "log-${local.lb_name}"

    dynamic "filter" {
      for_each = local.use_existing_s3 ? [1] : []
      content {
        prefix = "${local.lb_name}/"
      }
    }

    dynamic "expiration" {
      for_each = var.access_logs.expiration > 0 ? [1] : []
      content {
        days = var.access_logs.expiration
      }
    }

    status = "Enabled"

    dynamic "transition" {
      for_each = var.access_logs.glacier_transaction_days > 0 ? [1] : []
      content {
        days          = var.access_logs.glacier_transaction_days
        storage_class = "GLACIER"
      }
    }
  }
}