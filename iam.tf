data "aws_iam_policy_document" "default" {
  count = local.create_new_s3_bucket ? 1 : 0

  statement {
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${lookup(local.map_elb_aws_account, var.region)}:root"
      ]
    }

    actions = [
      "s3:PutObject",
    ]

    resources = [
      aws_s3_bucket.access_log[0].arn,
      "${aws_s3_bucket.access_log[0].arn}/*",
    ]
  }

  statement {
    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = [
      "s3:PutObject"
    ]
    resources = [
      aws_s3_bucket.access_log[0].arn,
      "${aws_s3_bucket.access_log[0].arn}/*"
    ]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }

  statement {
    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = [
      "s3:GetBucketAcl"
    ]
    resources = [
      aws_s3_bucket.access_log[0].arn
    ]
  }
}
