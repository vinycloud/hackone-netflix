locals {
  vpc_id              = " " #"vpc-012345678abcdefg"
  ami_id              = " " #"ami-xxxabcd0123456789"
  type_ec2            = " " #"t3.medium"
  key_pair            = " " #"aws-key_pair"
  name_security_group = " " #"security_group_portobank_asg"
  project             = " " #"nome-do-projeto"
  environment         = " " #"dev, hml ou prd"
  min_size            = 1
  max_size            = 1
  desired_capacity    = 1
}


data "aws_lb_target_group" "http" {
  name = "tg-dev-projeto-default-80" #nome do target group
}


data "aws_subnets" "all" {
  tags = {
    Name = "*private*"
  }
}

module "asg" {
  source              = "./module/asg"
  image_id            = local.ami_id
  instance_type       = local.type_ec2
  key_name            = local.key_pair
  inbound_roles       = var.inbound_roles
  vpc_id              = local.vpc_id
  volume_size         = var.volume_size
  min_size            = local.min_size
  max_size            = local.max_size
  desired_capacity    = local.desired_capacity
  name_security_group = local.name_security_group
  project             = local.project
  environment         = local.environment
  lb_target_group  = data.aws_lb_target_group.http.arn
  availability_zones = data.aws_subnets.all.ids
}




