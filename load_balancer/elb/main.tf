/*
terraform {
  backend "s3" {
    bucket         = "masf-brlink-dev-terraform-states-bucket" # it needs to be change
    key            = "infraestrutura/load-balancer.tfstate"
    region         = "eu-west-1"                                  # it needs to be change correct one it is ca-central-1
    dynamodb_table = "masf-brlink-dev-terraform-state-lock-table" # it needs to be change
    #profile        = "nome_do_projeto_dev"  # it needs to be change
  }
}
*/
locals {
  region = "us-east-1"
  vpc_id = "vpc-000xptoxptoxpto"
}

provider "aws" {
  region = local.region
}

data "aws_vpc" "selected" {
  id = local.vpc_id
}

data "aws_subnets" "all" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }
  tags = {
    Name = "*private*"
  }
}

module "loadbalancer_sample" {
  source = "../module"

  region = local.region
  type = var.type

  cluster_name     = "projeto"
  environment_name = "dev"

  vpc_id     = local.vpc_id
  subnet_ids = data.aws_subnets.all.ids
/*
  access_logs = {
    bucket                   = "lb-log-test"
    expiration               = 90
    glacier_transaction_days = 0
  }
*/
  listener_ports = [{
    port     = 80
    protocol = "TCP"
  },
  {
    port     = 443
    protocol = "TCP"
  }]
}
