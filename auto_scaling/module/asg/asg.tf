resource "aws_launch_configuration" "this" {
  name                 = "${var.project}-${var.environment}"
  image_id             = var.image_id
  instance_type        = var.instance_type
  key_name             = var.key_name
  iam_instance_profile = "AmazonSSMRoleForInstancesQuickSetup"
  security_groups      = [aws_security_group.this.id]
  root_block_device {
    delete_on_termination = true
    encrypted             = true
    iops                  = null
    volume_size           = var.volume_size
    volume_type           = "gp2"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "scalegroup" {  
  launch_configuration = aws_launch_configuration.this.name
  vpc_zone_identifier = var.availability_zones
  min_size            = var.min_size
  max_size            = var.max_size
  desired_capacity    = var.desired_capacity
  enabled_metrics     = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupTotalInstances"]
  metrics_granularity = "1Minute"
  target_group_arns = ["${var.lb_target_group}"]
  health_check_type = "ELB"
  force_delete      = true
  tag {
    key                 = "Name"
    value               = "${var.project}-${var.environment}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "autopolicy" {
  name                   = "terraform-autopolicy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.scalegroup.name
}

resource "aws_cloudwatch_metric_alarm" "cpualarm" {
  alarm_name          = "terraform-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.scalegroup.name
  }

  alarm_description = "This metric monitor EC2 instance cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.autopolicy.arn}"]
}

resource "aws_autoscaling_policy" "autopolicy-down" {
  name                   = "terraform-autoplicy-down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.scalegroup.name
}

resource "aws_cloudwatch_metric_alarm" "cpualarm-down" {
  alarm_name          = "terraform-alarm-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "10"

  alarm_description = "Métrica de monitoramento de utilização de CPU"
  alarm_actions     = ["${aws_autoscaling_policy.autopolicy-down.arn}"]

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.scalegroup.name
  }
}
