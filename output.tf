output "ip_address" {
  value = aws_instance.netflix.public_ip
}

output "private_ip" {
  value = aws_instance.netflix.private_ip
}

output "dns_name" {
  value = aws_instance.netflix.public_dns
}