variable "cluster_name" {
  type        = string
  description = "(Required) This will be the name of the cluster."
}

variable "region" {
  type        = string
  description = "(Required) The region This is the region where the resources will be applied."
}

variable "environment_name" {
  type        = string
  description = "(Required) The name of the environment. must be `dev`, `hml`, `prd`."

  validation {
    condition     = contains(["dev", "hml", "prd"], var.environment_name)
    error_message = "Valid values for var: environment_name are (dev, hml, prd)."
  }
}

variable "vpc_id" {
  type        = string
  description = "(Required) Vpc id"
}

variable "subnet_ids" {
  type        = list(string)
  description = "(Required) List of subnet ids"

  validation {
    condition     = length(var.subnet_ids) > 1
    error_message = " At least two subnets must be specified."
  }
}

variable "type" {
  type        = string
  description = "(Optional) The type of load balancer to create. Possible values are `application`, `gateway`, or `network`. The default value is `application`."
  /*
  default     = "application"
 
  validation {
    condition     = contains(["application", "gateway", "network"], var.type)
    error_message = "Type of load balancer must be   `application`, `gateway`, or `network`."
  }*/
}

variable "is_internal" {
  type        = bool
  description = "(Optional) If true, the LB will be internal. Default value is `true`"
  default     = true
}

variable "enable_http2" {
  type        = bool
  description = " (Optional) Indicates whether HTTP/2 is enabled in `application` load balancers. Defaults to `true`."
  default     = true
}

variable "tags" {
  type        = map(any)
  description = " (Optional) Map of tags to assign to the resource. If configured with a provider."
  default     = {}
}

variable "access_logs" {
  type = object({
    bucket                   = string,
    expiration               = number,
    glacier_transaction_days = number
  })
  description = "(Optional) If access_logs is `null` so the load balancer will not be store log in S3, otherwise the access_logs is required. \n if `bucket` is `null` this module will create a new bucket to store the log \n if `expiration` is 0 - only used when create a new bucket, the log will never be deleted. \n `glacier_transaction_days` is 0, the logger will never send the log to GLACIER - only used when create a new bucket. By security the defaults is { \n bucket = null, \n  expiration = 90, \n glacier_transaction_days = 30 \n }."
  default = {
    bucket                   = null
    expiration               = 90
    glacier_transaction_days = 30
  }
}

variable "security_groups_ids" {
  type        = list(string)
  description = "(Optional) List of security group that will be attached in LB if your `type` is `application`."
  default     = []
}

variable "security_group_ingress_ports" {
  type = list(object({
    from_port   = number,
    to_port     = number,
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "(Optional) All ports that will be attatched in security group that will be create if `security_groups_ids` is `null` or `empty` and  if your `type` is `application`., Default is export just port `443` to `0.0.0.0/0`"
  default = [{
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = ""
  }]
}


variable "listener_ports" {
  type = list(object({
    port     = number
    protocol = string
  }))
  description = "(Optional) Use this configuration to create a default listener ports to be shared to other target groups"
  default     = []

}
