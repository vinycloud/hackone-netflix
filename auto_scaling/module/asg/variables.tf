variable "image_id" {
  description = "ID da imagem EC2"
  type        = string
}

variable "instance_type" {
  description = "type EC2"
  type        = string
}

variable "key_name" {
  description = "Nome da key pair"
  type        = string
}

variable "vpc_id" {
  type        = string
  description = "vpc id"
}

variable "name_security_group" {
  type        = string
  description = "security group name"
}


variable "inbound_roles" {
  description = "security group tcp and/or udp"
  type        = map(any)
}

variable "volume_size" {
  description = "Definições de ebs"
  type        = number
}

variable "environment" {
  description = "Ambiente do projeto"
  type        = string
}

variable "project" {
  description = "Nome do projeto"
  type        = string
}

variable "min_size" {
  description = "Capacidade mínima de instancias"
  type        = number
}

variable "max_size" {
  description = "Capacidade máxima de instancias"
  type        = number
}

variable "desired_capacity" {
  description = "Capacidade desejada de instancias"
  type        = number
}

variable "availability_zones" {
  description = "Zonas de disponibilidade"

}

variable "lb_target_group" {
  type        = string
  description = "ARN do target group"
}

