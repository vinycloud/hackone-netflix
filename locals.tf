locals {
  lb_name                 = "lb-${var.cluster_name}-${var.environment_name}"
  request_security_groups = contains(["application"], var.type)

  create_security_group = local.request_security_groups && (var.security_groups_ids == null || length(var.security_groups_ids) <= 0)

  security_group = local.request_security_groups ? (local.create_security_group ? [aws_security_group.lb_sg[0].id] : var.security_groups_ids) : null

  use_access_log = try(var.access_logs.bucket, "not_use_access_log") != "not_use_access_log"

  use_existing_s3 = (
    try(var.access_logs.bucket, "") != "" &&
    try(var.access_logs.bucket, null) != null
  )

  create_s3_lifecycle = (
    local.use_access_log &&
    (try(var.access_logs.expiration, 0) > 0 || try(var.access_logs.glacier_transaction_days, 0) > 0)
  )

  create_new_s3_bucket = local.use_access_log && !local.use_existing_s3

  aws_s3_bucket = local.use_access_log ? (local.use_existing_s3 ? data.aws_s3_bucket.access_log[0].bucket : aws_s3_bucket.access_log[0].bucket) : null 

  tag_access_logs = local.use_access_log ? {
    AccessLogBucket     = local.aws_s3_bucket,
    AccessLogPrefix     = local.use_existing_s3 ? local.lb_name : null,
    AccessLogExpiration = try(var.access_logs.expiration, 0),
  } : {}

  tag_default = {
    Environment = var.environment_name,
    ClusterName = var.cluster_name,
  }

}

resource "random_id" "server" {
  byte_length = 8

}
