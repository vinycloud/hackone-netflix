data "aws_vpc" "selected" {
  id = var.vpc_id
}

resource "aws_security_group" "lb_sg" {
  count = local.create_security_group ? 1 : 0

  name        = "secgroup-lb-${var.cluster_name}-${var.environment_name}"
  description = "Security group to allow access in LB ${var.cluster_name} ${var.environment_name}"
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = toset(var.security_group_ingress_ports)

    content {
      from_port = ingress.value.from_port
      to_port   = ingress.value.to_port
      protocol  = ingress.value.protocol

      cidr_blocks = ingress.value.cidr_blocks
      description = ingress.value.description
    }
  }

  dynamic "ingress" {
    for_each = toset(var.listener_ports)

    content {
      from_port = ingress.value.port
      to_port   = ingress.value.port
      protocol  = "tcp"

      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  tags = merge(var.tags, local.tag_default)
}
